$(function(){
    $('.carousel').carousel({
            interval: 1000
        }); 
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});

$(function(){
    $('#exampleModal').on('show.bs.modal',function(e){
        console.log('el modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-success');
        $('#contactoBtn').addClass('btn-secondary');
        $('#contactoBtn').prop('disabled',true);
    });
   
    $('#exampleModal').on('hidden.bs.modal',function(e){
        console.log('el modal ya se ejecutó');
        $('#contactoBtn').removeClass('btn-secondary');
        $('#contactoBtn').addClass('btn-success');
        $('#contactoBtn').prop('disabled',false);
    });

});


