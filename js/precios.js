$(function(){
    $('#exampleModal').on('show.bs.modal',function(e){
        console.log('el modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-success');
        $('#contactoBtn').addClass('btn-secondary');
        $('#contactoBtn').prop('disabled',true);
    });
   
    $('#exampleModal').on('hidden.bs.modal',function(e){
        console.log('el modal ya se ejecutó');
        $('#contactoBtn').removeClass('btn-secondary');
        $('#contactoBtn').addClass('btn-success');
        $('#contactoBtn').prop('disabled',false);
    });

});