module.exports = function (grunt){

require('time-grunt')(grunt);
require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
});

grunt.initConfig({
    sass: {
        dist: {
            files: [{
                expand: true,
                cwd: 'css',
                src: ['*.scss'],
                dest: 'css',
                ext: '.css'
            }]
        }
    },
    watch:{
        files: ['css/*.scss'],
        tasks: ['css']
    },
    browserSync: {
        dev: {
            bsFiles: {
                src : ['css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
            },
            options: {
                watchTask: true,
                server: {
                    baseDir: './'
                }
            }
        }
    },

    imagemin: {
        dynamic: {
            files: [{
                expand: true,
                cwd: './',
                src: ['img/**/*.{png,jpg,gif}'],
                dest: 'dist/'
            }]
        }
    },

    copy: {
        html:{
            files:[{
                expand: true,
                dot: true,
                cwd: './',
                src: ['*.html'],
                dest: 'dist'
            }]
        },
        fonts:{
            files: [
                {
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }
            ]
        },
        images: {
            expand:     true,
            cwd:        './img',
            src:        ['*.svg'],
            dest:       'dist/img/',
        }
    },
    
    clean: {
        build: {
            src: ['dist/']
        }
    },

    cssmin:{
        dist: {}
    },

    uglify: {
        dist: {}
    },

    filerev: {
        options:{
            encoding: 'utf8',
            algorithm: 'md5',
            length: 8
        },
        release:{
            //filerev:release hashes(md5) all assets (images, js and css)
            // in dist directory
            files: [{
                src: [
                    'dist/js/*.js',
                    'dist/css/*.css'
                ]
            }]
        }
    },

    concat:{
        options: {
            separator: ';'
        },
        dist: {}
    },

    useminPrepare: {
        foo: {
            dest: 'dist',
            src: ['about.html', 'ccabello.html', 'contactanos.html', 'cpiel.html', 'csalud.html', 'index.html', 'precios.html']
        },
        options: {
            flow: {
                steps: {
                    css: ['cssmin'],
                    js: ['uglify']
                }
            },
            post: {
                css: [{
                    name: 'cssmin',
                    createConfig: function(contex, block){
                        var generated = contex.options.generated;
                        generated.options = {
                            keepSpecialComments: 0,
                            rebase: false
                        }
                    }
                }]
            }
        }
    },

    usemin:{
        html: ['dist/index.html', 'dist/about.html', 'dist/ccabello.html', 'dist/contactanos.html', 'dist/cpiel.html', 'dist/csalud.html', 'dist/precios.html',],
        options: {
            assetsDir: ['dist', 'dist/css', 'dist/js']
        }
    }
    
});


grunt.registerTask('css', ['sass']);
grunt.registerTask('default', ['browserSync', 'watch']);
grunt.registerTask('img:compress', ['imagemin']);
grunt.registerTask('build',[
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ])

};